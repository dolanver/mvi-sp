##BART poradí, jak pojmenovat diplomovou práci

Název práce: Generating titles from abstracts

Popis práce: Natrénovat neuronovou síť, která dostane na vstupu abstrakt vědecké práce a “vymyslí” pro ni název.
